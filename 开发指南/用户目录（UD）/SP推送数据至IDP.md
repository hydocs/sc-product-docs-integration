# SP推送数据至IDP
Integration提供一些UD同步的接口API（所有的API都是遵循SCIM协议的），SP通过调用这些API，可以将数据同步到IDP。SP在调用Integration接口时，必须传递access_token(具体使用方式请查看获取access_token)。

## 接口列表
[点击查看API文档](https://apizza.net/pro/#/project/e8f5d608348d74235241bf1fba6fc69b/browse)

密码：hy-integration-api-doc