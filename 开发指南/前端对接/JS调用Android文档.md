# JS代码调用原生android
## 关闭webview/H5页面
关闭webview/H5页面   cqgzw.finshWebView()  括号内支持传值  暂时只支持字符串

|		|				| 例子					|参数类型		|
|---	| ---			|						|				|
|方法	| finshWebView	|cqgzw.finshWebView()	|String(非必传)	|